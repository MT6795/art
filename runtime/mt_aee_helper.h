#ifdef CHECK_JNI_HAVE_AEE_FEATURE
#include "aee.h"
#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>
#include <string>
#include <stdio.h>
#include "runtime/art_method-inl.h"
namespace art{
#define CHECK_JNI_AEE_EXCEPTION(_msg, _className, _extraMsg)                                    \
    do {                                                                                            \
        std::string _aeeClassName(_className);                                                      \
        _aeeClassName += ".cpp";                                                                   \
        std::string _aeeMsg(_msg);                                                                  \
        _aeeMsg += "\nCRDISPATCH_FILE: " + _aeeClassName +                     \
        "\nCRDISPATCH_KEY: JNI_CHECK_ERROR" + "\n" + _extraMsg;                        \
        aee_system_exception(_aeeClassName.c_str(), NULL, DB_OPT_DEFAULT|DB_OPT_PROCESS_COREDUMP, _aeeMsg.c_str());    \
    } while (false)

inline void replaceAll(std::string& str, const std::string& from, const std::string& to) {
	if(from.empty())
		return;
	size_t start_pos = 0;
	while((start_pos = str.find(from, start_pos)) != std::string::npos) {
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
	}
}

inline std::vector<std::string> split(const std::string& source)
{
	std::istringstream ss(source);
	std::vector<std::string> vec((std::istream_iterator<std::string>(ss)), 
			std::istream_iterator<std::string>());
	return vec;
}

inline void run_aee(ArtMethod* current_method, std::string aee_msg){
	std::string aee_name(PrettyMethod(current_method));
	std::vector<std::string> aee_nameV1 = split(aee_name);
	if(aee_nameV1.size()>0){
		std::string pkg_name(aee_nameV1[1]);
		replaceAll(pkg_name,"."," ");
		std::vector<std::string> aee_nameV2 = split(pkg_name);
		std::string target_name;
		for(unsigned int i=0;i<aee_nameV2.size()-1;i++){
			target_name += aee_nameV2[i];
			if(i+1 != aee_nameV2.size()-1){
				target_name += "_";
			}
		}
		if((unsigned int)target_name.size()>0){
			CHECK_JNI_AEE_EXCEPTION (aee_msg, target_name, "please refer to http://wiki.mediatek.inc/display/ART/JNI");
		}
	}
}
}
#endif
